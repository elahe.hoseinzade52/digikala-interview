import "./App.scss";
import PLP from "./pages/productListPage/PLP";
import PDP from "./pages/productDetailsPage/PDP";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<PLP />} />
          <Route path=":type/:id/:title" element={<PDP />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
