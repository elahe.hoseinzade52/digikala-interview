import { useState, useEffect, useMemo } from "react";
import qs from "qs";

/**
 *@params {string} url
 *@params {object} params
 */
export const useFetch = ({ params, url }) => {
  const [state, setState] = useState({
    _tag: "INITIAL",
  });
  const [totalPages, setTotalPages] = useState(1);

  const params_ = useMemo(
    () => (params ? qs.stringify(params, { encode: false }) : ""),
    [params]
  );

  useEffect(() => {
    const f = () => {
      setState({ _tag: "LOADING" });
      fetch(`${url}?${params_}`)
        .then((x) => x.json())
        .then((data) => {
          setTotalPages(data.data.pager.total_pages);
          setState({ _tag: "DATA", data: data.data });
        })
        .catch((e) => {
          setState({ _tag: "ERROR", error: e });
        });
    };

    f();
  }, [params_, url]);

  return { params, state, totalPages };
};

useFetch.fold =
  ({ onData, onLoading, onError, onInitial }) =>
  (state) => {
    switch (state._tag) {
      case "INITIAL":
        return onInitial();
      case "DATA":
        return onData(state.data);
      case "LOADING":
        return onLoading();
      case "ERROR":
        return onError(state.error);
      default:
        throw Error("Unhandled tag");
    }
  };
