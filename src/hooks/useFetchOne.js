import { useState, useEffect } from "react";

/**
 *@params {string} url
 */
export const useFetchOne = ({ id, url }) => {
  const [state, setState] = useState({
    _tag: "INITIAL",
  });

  useEffect(() => {
    const f = () => {
      setState({ _tag: "LOADING" });
      fetch(`${url}/${id}/`)
        .then((x) => x.json())
        .then((data) => {
          setState({ _tag: "DATA", data: data.data });
        })
        .catch((e) => {
          setState({ _tag: "ERROR", error: e });
        });
    };

    f();
  }, [id, url]);

  return state;
};

useFetchOne.fold =
  ({ onData, onLoading, onError, onInitial }) =>
  (state) => {
    switch (state._tag) {
      case "INITIAL":
        return onInitial();
      case "DATA":
        return onData(state.data);
      case "LOADING":
        return onLoading();
      case "ERROR":
        return onError(state.error);
      default:
        throw Error("Unhandled tag");
    }
  };
