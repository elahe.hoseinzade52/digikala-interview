import { useEffect } from "react";

export const useScrollReachedBottom = (offset, clb) => {
  useEffect(() => {
    const onScroll = () => {
      const totalHeight =
        document.documentElement.scrollHeight - window.innerHeight;

      const scrollAt = totalHeight - (offset / 100) * totalHeight;

      if (window.scrollY >= scrollAt) {
        clb(true);
      } else {
        clb(false);
      }
    };

    window.addEventListener("scroll", onScroll);

    return () => {
      window.removeEventListener("scroll", onScroll);
    };
  }, [clb, offset]);
};
