import React from "react";
import { useParams } from "react-router-dom";
import { useFetchOne } from "../../hooks/useFetchOne";

const PDP = () => {
  const params = useParams();
  const id = params.id.split("-").at(-1);

  const data = useFetchOne({ url: "https://api.digikala.com/v1/product", id });

  return useFetchOne.fold({
    onData: ({ product }) => (
      <div className="product">
        <div className="product__image-box">
          <img
            className="product__image-box__main"
            src={product.images.main.url}
            alt={product.title_fa}
          />
        </div>
        <div className="product__info">
          <span>{product.title_fa}</span>
          <hr size="1" width="100%" color="#e0e0e2" />
        </div>
        <div className="product__seller-info"></div>
      </div>
    ),
    onError: () => <>Error!</>,
    onLoading: () => <>Loading...</>,
    onInitial: () => <>Wait</>,
  })(data);
};

export default PDP;
