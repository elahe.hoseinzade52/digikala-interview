import React, { useEffect, useMemo, useState } from "react";
import { useFetch } from "../../hooks/useFetch";
import { useScrollReachedBottom } from "../../hooks/useScroll";
import { Link } from "react-router-dom";
const ROW = 15;

const Products = ({ xs }) => {
  return (
    <div className="grid-container">
      <div className="products">
        {xs.map((x) => (
          <Link to={x.url.uri} key={x.id} className="products__item">
            <img
              className="products__item__image"
              src={x.images.main.url[0]}
              alt={x.title_fa}
            />
            <div className="products__item__title">
              <span>{x.title_fa}</span>
            </div>

            <span className="products__item__price">
              {x.default_variant.price.selling_price}
              {"  تومان   "}
            </span>
          </Link>
        ))}
      </div>
    </div>
  );
};

const PLP = () => {
  const [page, setPage] = useState(1);
  const [partialProducts, setPartialProducts] = useState([]);

  const params = useMemo(
    () => ({
      page,
      rows: ROW,
      price: { min: 90000, max: 100000 },
      has_selling_stock: 1,
      sort: 4,
      q: "سیب",
    }),
    [page]
  );

  const {
    params: fetchedParams,
    state,
    totalPages,
  } = useFetch({
    url: `https://api.digikala.com/v1/search/`,
    params,
  });

  useEffect(() => {
    useFetch.fold({
      onData: (data) => {
        if (
          // to prevent duplicate values
          !partialProducts.find(
            (x) => data.pager.current_page === x.pager.current_page
          )
        ) {
          setPartialProducts((p) => [...p, data]);
        }
      },
      onError: () => setPartialProducts([]),
      onLoading: () => {},
      onInitial: () => {},
    })(state);
  }, [fetchedParams.page, page, partialProducts, state]);

  useScrollReachedBottom(30, (reached) => {
    let fetching = false;

    if (reached && !fetching && page < totalPages) {
      fetching = true;
      setPage((p) => p + 1);
    } else if (!reached && fetching) {
      fetching = false;
    }
  });

  return partialProducts.map(({ products }, index) => (
    <Products xs={products} key={index} />
  ));
};

export default PLP;
